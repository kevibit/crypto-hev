const path = require('path')
const HTMLWebpackPlugin = require('html-webpack-plugin')

module.exports = {
    mode: 'development',
    entry: './src/main.jsx',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].[contenthash].bundle.js',
        clean: true,
    },
    resolve: {
        extensions: [".js", ".jsx"],
        alias: {
            'Utilities' : path.resolve(__dirname, 'src/Utilities/'),
            'Components' : path.resolve(__dirname, 'src/Components/'),
            'API' : path.resolve(__dirname, 'src/API/'),
            'Hooks' : path.resolve(__dirname, 'src/Hooks/'),
        },
    },
    devServer: {
        port: 3000,
        hot: true,
    },
    plugins: [
        new HTMLWebpackPlugin({
            template: './src/index.html',
        }),
    ],
    module: {
        rules: [
            {
                test: /\.s[ac]ss$/i,
                use: ["style-loader", "css-loader", "sass-loader"],
            },
            {
                test: /\.m?(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: [
                            '@babel/preset-react',
                        ]
                    }
                }
            }
        ]
    },
}