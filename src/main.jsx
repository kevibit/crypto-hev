import React from 'react'
import { render } from 'react-dom'
import './style.sass'

import App from "./App";

render(<App/>, document.getElementById('root'))