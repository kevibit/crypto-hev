import React from 'react';

import HourlyExchangeVolumeViewer from "./Components/HourlyExchangeVolumeViewer/HourlyExchangeVolumeViewer";

const App = () => {
    return (
        <div className='wrap'>
            <HourlyExchangeVolumeViewer/>
        </div>
    )
}

export default App;