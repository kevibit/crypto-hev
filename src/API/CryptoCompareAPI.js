import axios from "axios";

const BASE_URL = 'https://min-api.cryptocompare.com'
const API_KEY = '4c946f5be8c39a28fda0200f756834be8053b674e4fad0e87651c5e23f6fdf6d'

const HOURS_IN_DAY = 24;

export default class CryptoCompareAPI {
    static async getCurrencies() {
        const currencies = await axios.get(BASE_URL + '/data/blockchain/list', {
            params: {
                api_key: API_KEY,
            }
        }).then((response) => {
            return response.data;
        });

        let arrayData = [];

        if(currencies?.Response === 'Error') {
            throw new Error("(getCurrencies) Error: " + currencies.Message);
        } else {
            arrayData = Object.entries(currencies.Data).map((element) => element[1]);
        }

        return arrayData;
    }

    static async getVolume(sym = 'BTC', days = 1) {
        let hours = days * HOURS_IN_DAY;

        const dataVolume = await axios.get(BASE_URL + '/data/exchange/histohour', {
            params: {
                api_key: API_KEY,
                tsym: sym,
                limit: hours,
            }
        }).then((res) => res.data);

        let data = [];

        if(dataVolume?.Response === 'Error') {
            throw new Error("(getVolume) Error: " + dataVolume.Message);
        } else {
            data = dataVolume.Data;
        }

        return data;
    }
}