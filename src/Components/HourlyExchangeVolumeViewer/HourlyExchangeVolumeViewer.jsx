import React, {useState, useEffect} from 'react';
import './HourlyExchangeVolumeViewer.sass';

import Select from 'Components/UI/Select/Select';
import Button from 'Components/UI/Button/Button';
import RadioButton from 'Components/UI/Button/RadioButton';
import Loader from 'Components/UI/Loader/Loader';
import MiniLoader from 'Components/UI/Loader/MiniLoader';
import LineChart from 'Components/UI/Chart/LineChart';
import Alert from 'Components/UI/Alert/Alert';

import CryptoCompareAPI from 'API/CryptoCompareAPI';
import {useFetching} from 'Hooks/useFetching';

const CHART_PERIOD = {
    DAY:    { name: 'Day',      days: 1 },
    DAYS3:  { name: '3 Days',   days: 3 },
    WEEK:   { name: 'Week',     days: 7 },
    MONTH:  { name: 'Month',    days: 30 },
}

const periodItems = Object.keys(CHART_PERIOD).reduce((items, period, index) => {
    let newPeriod = {
        id: index,
        name: CHART_PERIOD[period].name,
        value: CHART_PERIOD[period]
    }
    items.push(newPeriod);
    return items;
}, []);

const DEFAULT_SYM = 'BTC';
const MILLISECONDS_IN_SECONDS = 1000;

const HourlyExchangeVolumeViewer = () => {
    const [currencies,  setCurrencies]  =   useState([]);
    const [currentCoin, setCurrentCoin] =   useState(DEFAULT_SYM);
    const [chartPeriod, setChartPeriod] =   useState(CHART_PERIOD.DAY);
    const [volumeData,  setVolumeData]  =   useState([]);
    const [refreshKey,  setRefreshKey]  =   useState(0);

    const [getCurrencies, currenciesIsLoading, loadCurrenciesError] = useFetching(async () => {
        const data = await CryptoCompareAPI.getCurrencies();
        setCurrencies(data);
    });

    const [getVolume, volumeIsLoading, loadVolumeError] = useFetching(async () => {
        const data = await CryptoCompareAPI.getVolume(currentCoin, chartPeriod.days);
        setVolumeData(arrayToChartData(data));
    });

    function arrayToChartData(arrayData) {
        return arrayData.map((item) => {
            return {
                date: new Date(item.time * MILLISECONDS_IN_SECONDS),
                value: item.volume,
                timestamp: item.time,
            };
        }).sort((a, b) => a.timestamp - b.timestamp);
    }

    useEffect(() => {
        getCurrencies();
    }, []);

    useEffect(() => {
        getVolume();
    }, [currentCoin, chartPeriod, refreshKey]);

    let refreshData = () => {
        setRefreshKey(refreshKey + 1);
    }

    const chartMargin = {
        top: 20,
        right: 20,
        bottom: 30,
        left: 60,
    };

    const chartData = {
        name: currentCoin,
        color: "#3D96FF",
        items: volumeData,
    };

    return (
        <div className="hev-viewer">
            <div className="hev-viewer_toolbar">
                <RadioButton
                    items={ periodItems }
                    setValue={ setChartPeriod }
                />
                <Button
                    active={ false }
                    name='Refresh'
                    onClick={ refreshData }
                />

                {
                    currenciesIsLoading
                        ?
                        <MiniLoader/>
                        :
                        <Select
                            items={ currencies }
                            defaultValue={ currentCoin }
                            setValue={ setCurrentCoin }
                            text='symbol'
                            value='symbol'
                        />
                }

            </div>

            <div className="hev-viewer_chart">
                {
                    (loadVolumeError || loadCurrenciesError)
                        ?
                        <Alert messages={[loadVolumeError, loadCurrenciesError]} />
                        :
                        volumeIsLoading
                            ?
                            <Loader/>
                            :
                            <LineChart
                                data={ chartData }
                                margin={ chartMargin }
                            />
                }

            </div>
        </div>
    );
};

export default HourlyExchangeVolumeViewer;