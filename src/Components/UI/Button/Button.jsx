import React from 'react';
import './Button.sass';

const Button = ({
    active = false,
    name = 'Button',
    onClick = () => {},
}) => {
    let buttonClass = active ? 'ui-button active' : 'ui-button';

    return (
        <button
            className={ buttonClass }
            onClick={ onClick }
        >
            {name}
        </button>
    );
};

export default Button;