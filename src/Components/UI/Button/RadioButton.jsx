import React, {useState}  from 'react';
import Button from "./Button";

const RadioButton = ({
     items = [],
     defaultValue = 0,
     setValue = () => {},
}) => {
    let [activeBtn, setActiveBtn] = useState(defaultValue);

    const onClickHandler = (btn) => {
        setActiveBtn(btn.id);
        setValue(btn.value);
    }

    return (
        <div className='ui-radioButton'>
            {items.map((btn) =>
               <Button
                   name={ btn.name }
                   active={ activeBtn === btn.id }
                   onClick={ () => onClickHandler(btn) }
                   key={ btn.id }
               />
            )}
        </div>
    );
};

export default RadioButton;