import React from 'react';

import './Alert.sass';

const Alert = ({messages}) => {

    console.log(messages);

    return (
        <div className='ui-alert'>
            {
                messages.map((message) =>
                    <p>{ message }</p>
                )
            }
        </div>
    );
};

export default Alert;