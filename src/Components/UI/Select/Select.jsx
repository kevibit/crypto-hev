import React from 'react';
import './Select.sass'

const Select = ({
    items = [],
    setValue,
    text,
    value,
    defaultValue
}) => {

    const onChangeHandler = (event) => {
        setValue(event.target.value);
    }

    return (
        <select
            className='ui-select'
            onChange={ onChangeHandler }
            value={ defaultValue }
        >
            {items.map((item) =>
                <option
                    value={ item[value] }
                    key={ item[value] }
                >
                    { item[text] }
                </option>
            )}
        </select>
    );
};

export default Select;