import React, { useRef, useState, useEffect } from "react";
import * as d3 from "d3";

import Utils from 'Utilities/NumberUtilities';

import './Chart.sass';

const LineChart = ({
    data,
    margin
}) => {
    const [chartWidth, setChartWidth] = useState(0);
    const [chartHeight, setChartHeight] = useState(0);

    const svgRef = useRef(null);

    useEffect(() => {
        setChartWidth(getWidth());
        setChartHeight(getHeight());
    }, []);

    const getWidth = () => {
        return svgRef.current.parentElement.offsetWidth;
    }

    const getHeight = () => {
        return svgRef.current.parentElement.offsetHeight;
    }

    const drawChart = () => {
        let width = getWidth() - margin.left - margin.right;
        let height = getHeight() - margin.top - margin.bottom;

        const svgEl = d3.select(svgRef.current);

        svgEl.selectAll("*").remove();

        const svg = svgEl
            .append("g")
            .attr("transform", `translate(${margin.left},${margin.top})`);

        const xScale = d3.scaleTime()
            .domain(d3.extent(data.items, (d) => d.date))
            .range([0, width]);

        const yScale = d3.scaleLinear()
            .domain(d3.extent(data.items, (d) => d.value))
            .range([height, 0]);

        const xAxis = d3.axisBottom(xScale)
            .ticks(5)
            .tickSize(-height + margin.bottom);

        const xAxisGroup = svg.append("g")
            .attr("transform", `translate(0, ${height})`)
            .call(xAxis);

        xAxisGroup.select(".domain").remove();

        xAxisGroup.selectAll("line").attr("stroke", "rgba(150, 150, 150, 0.5)");

        xAxisGroup.selectAll("text")
            .attr("opacity", 0.5)
            .attr("color", "black")
            .attr("font-size", "0.75rem");

        const yAxis = d3.axisLeft(yScale)
            .ticks(10)
            .tickSize(-width)
            .tickFormat((val) => `${Utils.shortLongNumber(val)}`);

        const yAxisGroup = svg.append("g").call(yAxis);

        yAxisGroup.select(".domain").remove();

        yAxisGroup.selectAll("line").attr("stroke", "rgba(150, 150, 150, 0.5)");

        yAxisGroup.selectAll("text")
            .attr("opacity", 0.5)
            .attr("color", "black")
            .attr("font-size", "0.75rem");

        const line = d3.line()
            .x((d) => xScale(d.date))
            .y((d) => yScale(d.value))
            .curve(d3.curveCardinal);

        svg.selectAll(".line")
            .data([data])
            .join("path")
            .attr("fill", "none")
            .attr("stroke", (d) => d.color)
            .attr("stroke-width", 2)
            .attr("d", (d) => line(d.items));


        let focus = svg.append("g")
            .attr("class", "focus")
            .style("display", "none");

        focus.append("circle")
            .attr("r", 5);

        focus.append("rect")
            .attr("class", "tooltip")
            .attr("width", 150)
            .attr("height", 50)
            .attr("x", 10)
            .attr("y", -22)
            .attr("rx", 4)
            .attr("ry", 4);

        focus.append("text")
            .attr("class", "tooltip-date")
            .attr("x", 15)
            .attr("y", -2);

        focus.append("text")
            .attr("class", "tooltip-value")
            .attr("x", 15)
            .attr("y", 18);

        const mouseMoveHandler = (mouseEvent) => {
            const pointerPosition = d3.pointer(mouseEvent);
            const hoveredDate = new Date(xScale.invert(pointerPosition[0]));

            const bisect = d3.bisector((item) => item.date).left;
            let itemIndex = bisect(data.items, hoveredDate);

            let {date, value} = data.items[itemIndex];

            let x = xScale(date);
            let y = yScale(value);

            focus.attr("transform", `translate( ${x} , ${y} )`);
            focus.select(".tooltip-date").text(date.toISOString().slice(0, 16).replace('T',' '));
            focus.select(".tooltip-value").text(value);
        }

        svg.append("rect")
            .attr("class", "overlay")
            .attr("width", width)
            .attr("height", height)
            .on("mouseover", () => focus.style("display", null))
            .on("mouseout", () => focus.style("display", "none"))
            .on("mousemove", mouseMoveHandler);

    }

    React.useEffect(() => {
        drawChart();
    }, [data]);

    return (
        <div className='ui-chart'>
            <svg ref={svgRef} width={chartWidth} height={chartHeight} />
        </div>
    );
};

export default LineChart;