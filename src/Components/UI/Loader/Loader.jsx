import React from 'react';
import './Loader.sass';

const Loader = () => {
    return (
        <div className='ui-loader_wrapper'>
            <div className="ui-loader">
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    );
};

export default Loader;