import React from 'react';
import './MiniLoader.sass';

const MiniLoader = () => {
    return (
        <div className="ui-mini-loader">
            <div></div>
            <div></div>
            <div></div>
        </div>
    );
};

export default MiniLoader;