export default class NumberUtilities {

    static shortLongNumber(number, digitsAfterDot = 2) {
        const COUNT_ZEROS_IN_THOUSAND = 3;
        const MULTIPLIERS = ['', 'k', 'M', 'B', 'T'];

        if (number < 1) return number;

        let numberMultiplierIndex = Math.floor(Math.log10(number) / COUNT_ZEROS_IN_THOUSAND);

        if (numberMultiplierIndex >= MULTIPLIERS.length) {
            numberMultiplierIndex = MULTIPLIERS.length - 1;
        }

        let divider = Math.pow(10, numberMultiplierIndex * COUNT_ZEROS_IN_THOUSAND);

        return (number / divider).toFixed(digitsAfterDot) + MULTIPLIERS[numberMultiplierIndex];
    }

}