# Crypto HEV

React app for view hourly exchange volume of cryptocurrencies

To run app: 
    
    npm start

To build app: 
    
    npm run build

See build in "./dist/index.html"